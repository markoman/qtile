#!/bin/sh

exec nitrogen --restore &
numlockx &
picom --config .config/picom/picom.conf &
